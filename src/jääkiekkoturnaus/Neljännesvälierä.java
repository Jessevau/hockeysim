/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jääkiekkoturnaus;

public class Neljännesvälierä {
    private final Lohko EkaNeljännesvälieräpari;
    private final Lohko TokaNeljännesvälieräpari;
    private final Lohko KolmasNeljännesvälieräpari;
    private final Lohko NeljäsNeljännesvälieräpari;
    
    public Neljännesvälierä(Lohko lohkoA, Lohko lohkoB, Lohko lohkoC, Lohko lohkoD){
        
        EkaNeljännesvälieräpari = new Lohko(lohkoA.getLohko().get(0), lohkoD.getLohko().get(1));
        TokaNeljännesvälieräpari = new Lohko(lohkoB.getLohko().get(0), lohkoC.getLohko().get(1));
        KolmasNeljännesvälieräpari = new Lohko(lohkoA.getLohko().get(1), lohkoD.getLohko().get(0));
        NeljäsNeljännesvälieräpari = new Lohko(lohkoB.getLohko().get(1), lohkoC.getLohko().get(0));
        
        lohkoA.nollaa(lohkoA.getLohko().get(0));
        lohkoD.nollaa(lohkoD.getLohko().get(1));
        lohkoB.nollaa(lohkoB.getLohko().get(0));
        lohkoC.nollaa(lohkoC.getLohko().get(1));
        lohkoA.nollaa(lohkoA.getLohko().get(1));
        lohkoD.nollaa(lohkoD.getLohko().get(0));
        lohkoB.nollaa(lohkoB.getLohko().get(1));
        lohkoC.nollaa(lohkoC.getLohko().get(0));
        
        
    }
    
    public Lohko getEkapari(){
        return EkaNeljännesvälieräpari;
    }
    
    public Lohko getTokapari(){
        return TokaNeljännesvälieräpari;
    }
    
    public Lohko getKolmaspari(){
        return KolmasNeljännesvälieräpari;
    }
    
    public Lohko getNeljäspari(){
        return NeljäsNeljännesvälieräpari;
    }
    
    public void pelaaNeljännesvälierät(){
        EkaNeljännesvälieräpari.pelaaYksi();
        TokaNeljännesvälieräpari.pelaaYksi();
        KolmasNeljännesvälieräpari.pelaaYksi();
        NeljäsNeljännesvälieräpari.pelaaYksi();
    }
    
    public void tulostaNeljännesvälierät(){
        EkaNeljännesvälieräpari.tulostaLohko();
        System.out.println("--------------------------------");
        TokaNeljännesvälieräpari.tulostaLohko();
        System.out.println("--------------------------------");
        KolmasNeljännesvälieräpari.tulostaLohko();
        System.out.println("--------------------------------");
        NeljäsNeljännesvälieräpari.tulostaLohko();
    }
}

