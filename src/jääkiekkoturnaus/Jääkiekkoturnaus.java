/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jääkiekkoturnaus;
import java.util.Scanner;
public class Jääkiekkoturnaus {

    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Kontrolleri kontrolleri = new Kontrolleri();
        System.out.print(" > Pelataanko turnaus (k/e)?: ");
        String komento = scan.nextLine();
        if ( komento.equals("k") ){ 
            System.out.println("");
            kontrolleri.start();
        } else {
            System.out.println(" > Lopetetaan ..... ");
            System.exit(0);
        }
    }
    
}
