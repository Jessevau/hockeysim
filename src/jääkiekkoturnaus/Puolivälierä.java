/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jääkiekkoturnaus;


public class Puolivälierä {
    private final Lohko ekaPuoli;
    private final Lohko tokaPuoli;
    
    public Puolivälierä(Neljännesvälierä a){
        ekaPuoli = new Lohko(a.getEkapari().getLohko().get(0), a.getKolmaspari().getLohko().get(0));
        tokaPuoli = new Lohko(a.getTokapari().getLohko().get(0), a.getNeljäspari().getLohko().get(0));
        
        a.getEkapari().nollaa(a.getEkapari().getLohko().get(0));
        a.getTokapari().nollaa(a.getTokapari().getLohko().get(0));
        a.getKolmaspari().nollaa(a.getKolmaspari().getLohko().get(0));
        a.getNeljäspari().nollaa(a.getNeljäspari().getLohko().get(0));
        
    }
    
    public Lohko getEka(){
        return ekaPuoli;
    }
    
    public Lohko getToka(){
        return tokaPuoli;
    }
    
    public void pelaaPuolivälierät(){
        ekaPuoli.pelaaYksi();
        tokaPuoli.pelaaYksi();
    }
    
    public void tulostaPuolivälierät(){
        ekaPuoli.tulostaLohko();
        System.out.println("--------------------------------");
        tokaPuoli.tulostaLohko();
    }
}
