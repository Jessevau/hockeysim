/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jääkiekkoturnaus;
import java.util.Scanner;
public class Kontrolleri {
    public static Scanner lukija = new Scanner(System.in, "ISO-8859-1");
    public void start(){
        Lohko lohkoA = new Lohko();
        Lohko lohkoB = new Lohko();
        Lohko lohkoC = new Lohko();
        Lohko lohkoD = new Lohko();
        String komento;
        System.out.println(" > JÄÄKIEKKOTURNAUS --- 16 OMAVALINTAISTA JOUKKUETTA");
        System.out.println(" > SYÖTÄ JOUKKUEET JA TAITOTASOT");
        for (int i = 1; i <= 16; i++) {
            System.out.print(" > "+i+". joukkue: ");
            String nimi = lukija.nextLine();
            System.out.print(" > "+i+". joukkueen taitotaso (50-100): ");
            int taitotaso = lukija.nextInt();
            lukija.nextLine();
            if ( taitotaso < 50 || taitotaso > 100 ){
                System.out.println(" > Virhe: taitotason oltava väliltä 50-100");
                System.out.print(" > Uusi taitotaso: ");
                taitotaso = lukija.nextInt();
                lukija.nextLine();
            }
            Joukkue joukkue = new Joukkue(nimi, taitotaso);
            System.out.println("");
            if ( i == 1 || i == 5 || i == 9 || i == 13 ){
                lohkoA.lisääLohkoon(lohkoA, joukkue);
            } else if ( i == 2 || i == 6 || i == 10 || i == 14 ){
                lohkoB.lisääLohkoon(lohkoB, joukkue);
            } else if ( i == 3 || i == 7 || i == 11 || i == 15 ){
                lohkoC.lisääLohkoon(lohkoC, joukkue);
            } else if ( i == 4 || i == 8 || i == 12 || i == 16 ){
                lohkoD.lisääLohkoon(lohkoD, joukkue);
            }
            
        }
        System.out.println(" > TULOSTETAAN LOHKOT....");
        System.out.println("LOHKO A ------------------------");
        lohkoA.tulostaLohko();
        System.out.println("LOHKO B ------------------------");
        lohkoB.tulostaLohko();
        System.out.println("LOHKO C ------------------------");
        lohkoC.tulostaLohko();
        System.out.println("LOHKO D ------------------------");
        lohkoD.tulostaLohko();
        
        System.out.println("");
        System.out.print(" > Pelataanko lohkot (k/e)?: ");
        komento = lukija.nextLine();
        if (komento.equals("k")){
            lohkoA.pelaaLohko();
            lohkoB.pelaaLohko();
            lohkoC.pelaaLohko();
            lohkoD.pelaaLohko();
            System.out.println("");
            System.out.println(" > LOHKOT PELATTIIN SEURAAVANLAISESTI ....");
            System.out.println("");
            System.out.println("LOHKO A ------------------------");
            lohkoA.tulostaLohko();
            System.out.println("LOHKO B ------------------------");
            lohkoB.tulostaLohko();
            System.out.println("LOHKO C ------------------------");
            lohkoC.tulostaLohko();
            System.out.println("LOHKO D ------------------------");
            lohkoD.tulostaLohko();
            System.out.println("");
            System.out.print(" > Jatketaanko neljännesvälieriin (k/e)?: ");
            String komentokaks = lukija.nextLine();
            System.out.println("");
            if ( komentokaks.equals("k") ){
                Neljännesvälierä nelkku = new Neljännesvälierä(lohkoA, lohkoB, lohkoC, lohkoD);
                System.out.println("NELJÄNNESVÄLIERÄPARIT ----------");
                nelkku.tulostaNeljännesvälierät();
                nelkku.pelaaNeljännesvälierät();
                System.out.println("");
                System.out.print(" > Pelataanko neljännesvälierät (k/e)?: ");
                String komentokolme = lukija.nextLine();
                System.out.println("");
                if ( komentokolme.equals("k") ){
                    System.out.println(" > PELATAAN NELJÄNNESVÄLIERÄT ....");
                    System.out.println("");
                    nelkku.tulostaNeljännesvälierät();
                    System.out.println("");
                    System.out.print(" > Jatketaanko puolivälieriin (k/e)?: ");
                    String komentoneljä = lukija.nextLine();
                    System.out.println("");
                    if ( komentoneljä.equals("k")){
                        Puolivälierä puoli = new Puolivälierä(nelkku);
                        System.out.println("PUOLIVÄLIERÄPARIT ---------------");
                        puoli.tulostaPuolivälierät();
                        System.out.println("");
                        System.out.print(" > Pelataanko puolivälierät (k/e)?: ");
                        String komentoviis = lukija.nextLine();
                        System.out.println("");
                        if ( komentoviis.equals("k")){
                            System.out.println(" > PELATAAN PUOLIVÄLIERÄT ....");
                            System.out.println("");
                            puoli.pelaaPuolivälierät();
                            puoli.tulostaPuolivälierät();
                            System.out.println("");
                            System.out.print(" > Jatketaanko finaaliin (k/e)?: ");
                            String komentokuus = lukija.nextLine();
                            System.out.println("");
                            if ( komentokuus.equals("k")){
                                Finaali finaali = new Finaali(puoli);
                                finaali.tulostaFinaali();
                                System.out.println("");
                                System.out.print(" > Pelataanko loppuottelu (k/e)?: ");
                                String komentoseiska = lukija.nextLine();
                                System.out.println("");
                                if ( komentoseiska.equals("k")){
                                    System.out.println(" > PELATAAN FINAALI ....");
                                    System.out.println("");
                                    finaali.pelaaFinaali();
                                    finaali.tulostaTuloksella();
                                    System.out.println("");
                                    System.out.println(" > Turnaus on päättynyt! Voittajaksi selviytyi "+finaali.getVoittaja());
                                }
                            } else {
                                System.out.println(" > Lopetetaan...");
                                System.exit(0);
                            }
                        } else {
                            System.out.println(" > Lopetetaan...");
                            System.exit(0);
                        }
                    } else {
                        System.out.println(" > Lopetetaan...");
                        System.exit(0);
                    }
                } else {
                    System.out.println(" > Lopetetaan...");
                    System.exit(0);
                }
            } else {
                System.out.println(" > Lopetetaan...");
                System.exit(0);
            }
        } else {
            System.out.println(" > Lopetetaan...");
            System.exit(0);
        }
    }
}
