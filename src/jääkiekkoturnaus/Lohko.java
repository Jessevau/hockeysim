
package jääkiekkoturnaus;

import java.util.ArrayList;
import java.util.Collections;


public class Lohko {
    private ArrayList<Joukkue> lohko = new ArrayList();
    private Peli peli = new Peli();
    
    public Lohko(Joukkue j, Joukkue i, Joukkue k, Joukkue m){
        lohko.add(i);
        lohko.add(j);
        lohko.add(k);
        lohko.add(m);
    }
    
    public Lohko(){
        
    }
    
    public Lohko(Joukkue j, Joukkue i){
        lohko.add(j);
        lohko.add(i);
    }
    
    public ArrayList<Joukkue> getLohko(){
        return this.lohko;
    }
    
    public void lisääLohkoon(Lohko lohko, Joukkue joukkue){
        lohko.getLohko().add(joukkue);
    }
    
    
    public void järjestäLohko(){
        int maxInd;
        for (int i = 0; i < lohko.size(); i++) {
            maxInd = suurimmanIndeksi(lohko, i);
            vaihda(lohko, i, maxInd);
        }
    }
    
    public static int suurimmanIndeksi(ArrayList<Joukkue> lohko, int i){
        int indeksi = i;
        int suurin = lohko.get(indeksi).getVoitot();
        while (i < lohko.size() ) {
            if ( lohko.get(i).getVoitot() > suurin ){
                suurin = lohko.get(i).getVoitot();
                indeksi = i;
            }
            i++;
        }
        return indeksi;
    }
    
    public static void vaihda(ArrayList Lohko, int i, int j){
        Collections.swap(Lohko, i, j);
    }
    
    public void tulostaLohko(){
        järjestäLohko();
        System.out.println("JOUKKUE                  W     L");
        for ( Joukkue j : lohko ){
            j.tulostaJoukkue();
        }
    }
    
    public void nollaa(Joukkue j){
        j.setVoitot();
        j.setHäviöt();
    }
    
    public void pelaaLohko(){
        for (int i = 0; i < 2; i++) {
            peli.pelaaPeli(lohko.get(0), lohko.get(1));
            peli.pelaaPeli(lohko.get(2), lohko.get(3));
        }
        for (int i = 0; i < 2; i++) {
            peli.pelaaPeli(lohko.get(0), lohko.get(2));
            peli.pelaaPeli(lohko.get(1), lohko.get(3));
        }
        for (int i = 0; i < 2; i++) {
            peli.pelaaPeli(lohko.get(0), lohko.get(3));
            peli.pelaaPeli(lohko.get(1), lohko.get(2));
        }
    }
    
    public void pelaaYksi(){
        peli.pelaaPeli(lohko.get(0), lohko.get(1));
    }
}
