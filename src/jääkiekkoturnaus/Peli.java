
package jääkiekkoturnaus;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class Peli {
    Random r = new Random();
    
    public void pelaaPeli(Joukkue j, Joukkue k){
        int jTaito = j.getTaitotaso();
        int kTaito = k.getTaitotaso();
        int rajaJ = 50;
        int rajaK = 50;
        int tasaRaja = 50;
        if ( jTaito > kTaito ){
            rajaJ += jTaito-kTaito;
        } else if ( kTaito > jTaito ) {
            rajaK += kTaito-jTaito;
        } else {
            tasaRaja += 0;
        }
        
        int luku = r.nextInt(101);
        if ( jTaito != kTaito ){
            if ( rajaK == 50 ){
                if ( luku >= 0 && luku < rajaJ ){
                    j.lisääVoitto();
                    k.lisääHäviö();
                } else {
                    k.lisääVoitto();
                    j.lisääHäviö();
                }
            }
            if ( rajaJ == 50 ){
                if ( luku >= 0 && luku < rajaK ){
                    k.lisääVoitto();
                    j.lisääHäviö();
                } else {
                    j.lisääVoitto();
                    k.lisääHäviö();
                }
            }
        } else {
            if ( luku >= 0 && luku < 50 ){
                k.lisääVoitto();
                j.lisääHäviö();
            }
            if ( luku >= 50 && luku <= 100){
                j.lisääVoitto();
                k.lisääHäviö();
            }
        }
    }
}
