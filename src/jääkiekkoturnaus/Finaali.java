/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jääkiekkoturnaus;

import java.util.Random;


public class Finaali {
    private final Lohko finaali;
    private String[] tulokset = {"2-1", "6-1", "1-0", "3-2", "3-0", "5-4", "2-0", "4-1", "4-3"};
    Random r = new Random();
    
    public Finaali(Puolivälierä a){
        Lohko eka = a.getEka();
        Lohko toka = a.getToka();
        
        finaali = new Lohko(eka.getLohko().get(0), toka.getLohko().get(0));
    }
    
    public void pelaaFinaali(){
        finaali.pelaaYksi();
        finaali.järjestäLohko();
    }
    
    public void tulostaFinaali(){
        System.out.println("FINAALIPARI");
        System.out.println(finaali.getLohko().get(0).getNimi()+" - "+finaali.getLohko().get(1).getNimi());
    }
    
    public void tulostaTuloksella(){
        int luku = r.nextInt(8);
        System.out.println("FINAALIPARI");
        System.out.println(finaali.getLohko().get(0).getNimi()+" "+tulokset[luku]+" "+finaali.getLohko().get(1).getNimi());
        System.out.println("");
        System.out.println(finaali.getLohko().get(0).getNimi()+" voittaa turnauksen!");
    }
    
    public String getVoittaja(){
        return finaali.getLohko().get(0).getNimi();
    }
}
