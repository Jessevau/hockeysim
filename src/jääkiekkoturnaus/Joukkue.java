/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jääkiekkoturnaus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Joukkue {
    private final String nimi;
    private final int taitoTaso;
    private int voitot;
    private int häviöt;
    Map<Joukkue, List<String>> otteluHistoria = new HashMap();
    
    
    public Joukkue(String nimi, int taitotaso){
        this.nimi = nimi;
        this.taitoTaso = taitotaso;
        this.voitot = 0;
        this.häviöt = 0;
    }
    
    public void lisääVoitto(){
        this.voitot++;
    }
    
    public void lisääHäviö(){
        this.häviöt++;
    }
    
    public int getVoitot(){
        return this.voitot;
    }
    
    public void setVoitot(){
        this.voitot = 0;
    }
    
    public void setHäviöt(){
        this.häviöt = 0;
    }
    
    public int getHäviöt(){
        return this.häviöt;
    }
    
    public String getNimi(){
        return this.nimi;
    }
    
    public int getTaitotaso(){
        return this.taitoTaso;
    }
    
    public void tulostaJoukkue(){
        System.out.printf("%-20s%6s%6s\n",this.nimi, this.voitot, this.häviöt);
    }
}
